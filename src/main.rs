use std::env;
mod cli;
mod lib;
use std::fs::read_to_string;

fn main() {
    //clap stuff
    let app = cli::app().get_matches();
    //try getting key from key.txt, envvar $VULTR_API_KEY, cli arg
    let key = read_to_string("key.txt")
        .map(|x| x.trim().to_string())
        .or(env::var("VULTR_API_KEY"))
        .or(app.value_of("Vultr API Key").map(|key| key.to_string()).ok_or("err"))
        .expect("API key not found. Place it in key.txt, or set $VULTR_API_KEY, or specify with -k/--key");
    let key = &key;

    let menu0 = vec![
        "0. account",
        "1. apps",
        "2. api key",
        "3. backups",
        "4. bare metal",
        "5. block storage",
        "6. dns",
        "7. firewall",
        "8. iso files",
        "9. network",
        "10. os",
        "11. plans",
        "12. regions",
        "13. reserved ip",
        "14. servers",
        "15. snapshot",
        "16. ssh keys",
        "17. scripts",
        "18. users",
    ];

    loop {
        let selection0 = cli::selection_menu(&menu0, "\nVultrAdm");

        //println!("{}", selection0);

        match selection0 {
            //account
            0 => lib::account::print_info(key),
            //api key
            2 => lib::apikey::print_info(key),
            //backups
            3 => lib::backup::print_list(key),
            //servers
            14 => lib::servers::servers_view(key),
            //SSH Keys
            16 => lib::sshkey::keys_view(key),
            _ => (),
        }
    }
}
