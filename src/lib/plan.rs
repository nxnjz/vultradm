use crate::cli;
use crate::lib::http;
use serde::{Deserialize, Serialize};
use serde_json::{from_value, Value};

#[derive(Serialize, Deserialize, Clone)]
#[allow(non_snake_case)]
pub struct Plan {
    pub VPSPLANID: String,
    pub name: String,
    pub vcpu_count: String,
    pub ram: String,
    pub disk: String,
    pub bandwidth: String,
    pub price_per_month: String,
    pub plan_type: String,
    pub available_locations: Vec<u64>,
}

impl Plan {
    pub fn display_string(&self) -> String {
        format!(
            "‣ [{}] {} {}vCPU USD{}",
            self.plan_type, self.name, self.vcpu_count, self.price_per_month
        )
    }
}

//pub fn plans_menu() {
//    let menu: Vec<String> = [
//        "0. all virtual",
//        "1. vc2 - cloud compute",
//        "2. vc2z - high frequency cloud compute",
//        "3. vdc2 - dedicated vCPU",
//        "4. bare metal",
//    ]
//    .iter()
//    .map(|x| x.to_string())
//    .collect();
//    let selection = cli::selection_menu(&menu, "");
//
//    match selection {
//        //all virtual
//        0 => print_list_vps(),
//        1 => print_list_vc2(),
//        2 => print_list_vc2z(),
//        3 => print_list_vdc2(),
//        4 => print_list_metal(),
//        _ => (),
//    }
//}

//id_list: avail vpsplanids in region
pub fn plans_matching_ids(key: &str, id_list: Vec<u64>) -> Vec<Plan> {
    let txt: String = http::auth_get("https://api.vultr.com/v1/plans/list?type=all", key);
    //println!("{}", txt);
    let json: Value = serde_json::from_str(&txt).unwrap();
    let mut matching_plans = Vec::new();
    for i in json.as_object().expect("crash").values() {
        let j = i.clone();
        matching_plans.push(from_value(j).expect("deser crash"));
    }
    matching_plans.retain(|x: &Plan| id_list.contains(&x.VPSPLANID.parse::<u64>().unwrap()));
    matching_plans
}

//id_list: avail vpsplanids in specific egion
pub fn select_plan(key: &str, id_list: Vec<u64>) -> Plan {
    let mut plans = plans_matching_ids(key, id_list);
    let menu: Vec<String> = plans.iter().map(|plan| plan.display_string()).collect();
    let selection = cli::selection_menu(&menu, "\nVultrAdm:Servers:Deploy:Select Plan");
    return plans.remove(selection);
}

//pub fn print_list_vps() {
//    let txt: String = http::pub_get("https://api.vultr.com/v1/plans/list".to_string());
//    let json: Value = serde_json::from_str(&txt).unwrap();
//    println!(
//        "{: <8} {: <10} {: <4} {: <8} {}",
//        "ID", "TYPE", "vCPU", "USD/MON", "SPECS"
//    );
//    for value in json.as_object().unwrap().values() {
//        println!(
//            "{: <8} {: <10} {: <4} {: <8} {}",
//            value["VPSPLANID"].as_str().unwrap_or("error"),
//            value["plan_type"]
//                .as_str()
//                .unwrap_or("error")
//                .replace("HIGHFREQUENCY", "HIGHFREQ")
//                .replace("DEDICATED", "DEDICPU"),
//            value["vcpu_count"].as_str().unwrap_or("error"),
//            value["price_per_month"].as_str().unwrap_or("error"),
//            value["name"].as_str().unwrap_or("error"),
//        );
//    }
//}
//pub fn print_list_vc2() {
//    let txt: String = http::pub_get("https://api.vultr.com/v1/plans/list_vc2".to_string());
//    let json: Value = serde_json::from_str(&txt).unwrap();
//    println!("{: <10} {}", "ID", "NAME");
//    for value in json.as_object().unwrap().values() {
//        println!(
//            "{: <8} {: <10} {: <4} {: <8} {}",
//            value["VPSPLANID"].as_str().unwrap_or("error"),
//            value["plan_type"].as_str().unwrap_or("error"),
//            value["vcpu_count"].as_str().unwrap_or("error"),
//            value["price_per_month"].as_str().unwrap_or("error"),
//            value["name"].as_str().unwrap_or("error"),
//        );
//    }
//}
//pub fn print_list_vc2z() {
//    let txt: String = http::pub_get("https://api.vultr.com/v1/plans/list_vc2z".to_string());
//    let json: Value = serde_json::from_str(&txt).unwrap();
//    println!("{: <10} {}", "ID", "NAME");
//    for value in json.as_object().unwrap().values() {
//        println!(
//            "{: <8} {: <10} {: <4} {: <8} {}",
//            value["VPSPLANID"].as_str().unwrap_or("error"),
//            value["plan_type"]
//                .as_str()
//                .unwrap_or("error")
//                .replace("HIGHFREQUENCY", "HIGHFREQ"),
//            value["vcpu_count"].as_str().unwrap_or("error"),
//            value["price_per_month"].as_str().unwrap_or("error"),
//            value["name"].as_str().unwrap_or("error"),
//        );
//    }
//}
//pub fn print_list_vdc2() {
//    let txt: String = http::pub_get("https://api.vultr.com/v1/plans/list_vdc2".to_string());
//    let json: Value = serde_json::from_str(&txt).unwrap();
//    println!("{: <10} {}", "ID", "NAME");
//    for value in json.as_object().unwrap().values() {
//        println!(
//            "{: <8} {: <10} {: <4} {: <8} {}",
//            value["VPSPLANID"].as_str().unwrap_or("error"),
//            value["plan_type"]
//                .as_str()
//                .unwrap_or("error")
//                .replace("DEDICATED", "DEDICPU"),
//            value["vcpu_count"].as_str().unwrap_or("error"),
//            value["price_per_month"].as_str().unwrap_or("error"),
//            value["name"].as_str().unwrap_or("error"),
//        );
//    }
//}
//pub fn print_list_metal() {
//    let txt: String = http::pub_get("https://api.vultr.com/v1/plans/list_baremetal".to_string());
//    let json: Value = serde_json::from_str(&txt).unwrap();
//    println!("{: <10} {}", "ID", "NAME");
//    for value in json.as_object().unwrap().values() {
//        println!(
//            "{: <10} {}",
//            value["METALPLANID"].as_str().unwrap_or("error"),
//            value["name"].as_str().unwrap_or("error")
//        );
//    }
//}
