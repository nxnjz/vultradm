use crate::lib::http;
use serde_json::Value;

pub fn print_info(key: &str) {
    let txt: String = http::auth_get("https://api.vultr.com/v1/auth/info", key);
    let json: Value = serde_json::from_str(&txt).unwrap();
    println!(
        "NAME: {}\nEMAIL: {}\nACLs: {}",
        json["name"].as_str().unwrap(),
        json["email"].as_str().unwrap(),
        json["acls"]
            .as_array()
            .unwrap()
            .into_iter()
            .map(|x| x.as_str().unwrap())
            .collect::<Vec<&str>>()
            .join("\n      "),
    );
}
