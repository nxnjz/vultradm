use crate::cli;
use crate::lib::func::sleep;
use reqwest;
use reqwest::{header, header::HeaderName, Client, Response};
use std::collections::HashMap;

static USER_AGENT: &str = "VultrAdm/0.0.1";

pub enum Method {
    post,
    get,
}

enum Action {
    retry(u64, String),
    msg(String),
    prompt_retry(String),
    ok,
}

pub fn pub_get(url: &str) -> String {
    let client = Client::new();
    let mut resp = client.get(url).send().unwrap();
    resp.text().unwrap_or("error".to_string())
}

pub fn auth_get(url: &str, key: &str) -> String {
    let mut headers = header::HeaderMap::new();
    let api_header = HeaderName::from_bytes(b"API-Key").unwrap();
    //println!("{}", key);
    headers.insert(api_header, key.parse().unwrap());
    let client = Client::builder().default_headers(headers).build().unwrap();
    let mut resp = client.get(url).send().unwrap();
    let returnv = resp.text().expect("http error").to_string();
    //println!("{}", returnv);
    returnv
}

pub fn post_success(url: &str, key: &str, data: HashMap<&str, String>) -> bool {
    let mut headers = header::HeaderMap::new();
    let api_header = HeaderName::from_bytes(b"API-Key").unwrap();
    headers.insert(api_header, key.parse().unwrap());
    let client = Client::builder().default_headers(headers).build().unwrap();
    let resp = client.post(url).form(&data).send().unwrap();
    resp.status().is_success()
}

pub fn post(url: &str, key: &str, data: HashMap<&str, String>) -> Response {
    let mut headers = header::HeaderMap::new();
    let api_header = HeaderName::from_bytes(b"API-Key").unwrap();
    headers.insert(api_header, key.parse().unwrap());
    let client = Client::builder().default_headers(headers).build().unwrap();
    let resp = client.post(url).form(&data).send().unwrap();
    resp
}

pub fn auth_get_query(url: &str, key: &str, data: HashMap<&str, String>) -> String {
    let mut headers = header::HeaderMap::new();
    let api_header = HeaderName::from_bytes(b"API-Key").unwrap();
    headers.insert(api_header, key.parse().unwrap());
    let client = Client::builder().default_headers(headers).build().unwrap();
    let mut resp = client.get(url).query(&data).send().unwrap();
    resp.text().unwrap_or("error".to_string()).to_string()
}
//fn api_get(url: &str, key: Option<&str>, data: HashMap<&str, String>) -> Response {
//    let mut headers = header::HeaderMap::new();
//    if key.is_some() {
//        let api_header = HeaderName::from_bytes(b"API-Key").unwrap();
//        headers.insert(api_header, key.unwrap().parse().unwrap());
//    }
//    let client = Client::builder().default_headers(headers).build().unwrap();
//    let req_builder = client.get(url).query(&data);
//    //let mut resp = client.get(url).query(&data).send().unwrap();
//    let mut resp = req_builder.try_clone().unwrap().send().unwrap();
//    let mut response_ok = check_response_ok(&resp);
//    while response_ok.is_err() {
//        let error = response_ok.err().unwrap();
//        let retry_delay = error.0;
//        println!("{}", error.1);
//        sleep(retry_delay);
//        //resp = client.post(url).form(&data).send().unwrap();
//        resp = req_builder.try_clone().unwrap().send().unwrap();
//        response_ok = check_response_ok(&resp);
//    }
//    resp
//}
//fn api_post0(url: &str, key: Option<&str>, data: HashMap<&str, String>) -> Response {
//    let mut headers = header::HeaderMap::new();
//    if key.is_some() {
//        let api_header = HeaderName::from_bytes(b"API-Key").unwrap();
//        headers.insert(api_header, key.unwrap().parse().unwrap());
//    }
//    let client = Client::builder().default_headers(headers).build().unwrap();
//    let req_builder = client.post(url).form(&data);
//    //println!("{:?}", req_builder);
//    //let mut resp = client.post(url).form(&data).send().unwrap();
//    let mut resp = req_builder.try_clone().unwrap().send().unwrap();
//    //println!("{:?}", resp);
//    let mut response_ok = check_response_ok(&resp);
//    while response_ok.is_err() {
//        let error = response_ok.err().unwrap();
//        let retry_delay = error.0;
//        println!("{}", error.1);
//        sleep(retry_delay);
//        //resp = client.post(url).form(&data).send().unwrap();
//        resp = req_builder.try_clone().unwrap().send().unwrap();
//        response_ok = check_response_ok(&resp);
//    }
//    resp
//}
//
fn api_get(url: &str, key: Option<&str>, data: HashMap<&str, String>) -> Response {
    let mut headers = header::HeaderMap::new();
    if key.is_some() {
        let api_header = HeaderName::from_bytes(b"API-Key").unwrap();
        headers.insert(api_header, key.unwrap().parse().unwrap());
        headers.insert(
            header::USER_AGENT,
            header::HeaderValue::from_static(USER_AGENT),
        );
    }
    let client = Client::builder().default_headers(headers).build().unwrap();
    let req_builder = client.get(url).query(&data);
    println!("{:?}", req_builder);
    let mut resp;
    loop {
        resp = req_builder.try_clone().unwrap().send().unwrap();
        println!("{:?}", resp);
        let response_ok = check_response_ok(&resp);
        match response_ok {
            Action::ok => break,
            Action::retry(delay, msg) => cli::sleep_with_msg(delay, &msg),
            _ => panic!("sum ting wong"),
        }
    }
    resp
}
fn api_post(url: &str, key: Option<&str>, data: HashMap<&str, String>) -> Response {
    let mut headers = header::HeaderMap::new();
    if key.is_some() {
        let api_header = HeaderName::from_bytes(b"API-Key").unwrap();
        headers.insert(api_header, key.unwrap().parse().unwrap());
        headers.insert(
            header::USER_AGENT,
            header::HeaderValue::from_static(USER_AGENT),
        );
    }
    let client = Client::builder().default_headers(headers).build().unwrap();
    let req_builder = client.post(url).form(&data);
    println!("{:?}", req_builder);
    let mut resp;
    loop {
        resp = req_builder.try_clone().unwrap().send().unwrap();
        println!("{:?}", resp);
        let response_ok = check_response_ok(&resp);
        match response_ok {
            Action::ok => break,
            Action::retry(delay, msg) => cli::sleep_with_msg(delay, &msg),
            _ => panic!("sum ting wong"),
        }
    }
    resp
}
// Ok(true) or Err((retry_delay, retry_msg))
fn check_response_ok(resp: &Response) -> Action {
    let status_code = resp.status().as_u16();
    match status_code {
        400 => panic!("HTTP error 400, invalid API location. Report this to VultrAdm devs."),
        403 => panic!("HTTP error 403, invalid or missing API key. Check that your API key is present and matches your assigned key."),
        405 => panic!("Invalid HTTP method. Report this to VultrAdm devs."),
        200 => return Action::ok,
        503 => return Action::retry(2,"API rate limit hit, retrying in 2 seconds...".to_string()),
        other => {
            let msg = format!("Unknown response status: {}", other);
            return Action::msg(msg)
        }
    }
}

pub fn api_req(
    method: Method,
    url: &str,
    key: Option<&str>,
    data: Option<HashMap<&str, String>>,
) -> Response {
    match method {
        Method::post => return api_post(url, key, data.unwrap_or(HashMap::new())),
        //fix this
        Method::get => return api_get(url, key, data.unwrap_or(HashMap::new())),
    }
}
//pub fn post2(url: &str, key: &str, data: HashMap<&str, String>) -> Response {
//    let mut headers = header::HeaderMap::new();
//    let api_header = HeaderName::from_bytes(b"API-Key").unwrap();
//    headers.insert(api_header, key.parse().unwrap());
//    let client = Client::builder().default_headers(headers).build().unwrap();
//    let resp = client.post(url).form(&data).send().unwrap();
//    resp
//}
//pub fn vultr_api_text(http_res: Response) -> String {
//    let res = vultr_api(http_res).text().expect("unexpected error");
//    res
//}
//
//pub fn vultr_api(api_res: Response) -> Response {
//    if api_res.is_ok() {
//        return api_res.unwrap();
//    }
//    loop {
//        let err = api_res.err().unwrap();
//    }
//}
