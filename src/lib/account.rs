use crate::lib::http;
use serde_json::Value;

pub fn print_info(key: &str) {
    let txt: String = http::auth_get("https://api.vultr.com/v1/account/info", key);
    let json: Value = serde_json::from_str(&txt).unwrap();
    println!(
        "Balance: {}\nPending Charges:  {}\nLast Payment: {}\nLast Payment Amount: {}",
        json["balance"].as_str().unwrap_or("error"),
        json["pending_charges"].as_str().unwrap_or("error"),
        json["last_payment_date"].as_str().unwrap_or("error"),
        json["last_payment_amount"].as_str().unwrap_or("error")
    );

    //pub fn test_key(key: &str) {
    //let

    //let mut table = Table::new("{:<} {:<} {:<} {:<}");

    //table.add_row(
    //Row::new()
    //.with_cell("Balance")
    //.with_cell("Pending Charges")
    //.with_cell("Last Payment Date")
    //.with_cell("Last Payment Amount"),
    //);

    //table.add_row(
    //Row::new()
    //.with_cell(json["balance"].as_str().unwrap_or("error"))
    //.with_cell(json["pending_charges"].as_str().unwrap_or("error"))
    //.with_cell(json["last_payment_date"].as_str().unwrap_or("error"))
    //.with_cell(json["last_payment_amount"].as_str().unwrap_or("error")),
    //);

    //println!("{}", table);
}
