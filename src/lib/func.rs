use std::{thread, time};

pub fn s(stri: &str) -> String {
    stri.to_string()
}

pub fn sleep(secs: u64) {
    let dur = time::Duration::from_secs(secs);
    thread::sleep(dur);
}
