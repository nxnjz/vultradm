use crate::cli::confirm;
use crate::lib::app::App;
use crate::lib::http::{auth_get_query, post, post_success};
use crate::lib::os::Os;
use crate::lib::plan;
use crate::lib::plan::Plan;
use crate::lib::servers::Server;
//use serde::{Deserialize, Serialize};
use serde_json::{from_value, Value};
use std::collections::HashMap;

impl Server {
    pub fn reboot(&self, key: &str) -> bool {
        let mut data = HashMap::new();
        data.insert("SUBID", self.SUBID.clone().unwrap().to_string());
        post_success("https://api.vultr.com/v1/server/reboot", key, data)
    }
    pub fn reinstall(&self, key: &str) -> bool {
        if !confirm(&format!(
            "Reinstall server {}? All data will be permanently lost.",
            self.label.clone().unwrap()
        )) {
            return false;
        }
        let mut data = HashMap::new();
        data.insert("SUBID", self.SUBID.clone().unwrap().to_string());
        post_success("https://api.vultr.com/v1/server/reinstall", key, data)
    }
    pub fn halt(&self, key: &str) -> bool {
        if !confirm(&format!(
            "Stop server {}? This is hard power off. Billing for this instance will not stop.",
            self.label.clone().unwrap()
        )) {
            return false;
        }
        let mut data = HashMap::new();
        data.insert("SUBID", self.SUBID.clone().unwrap().to_string());
        post("https://api.vultr.com/v1/server/halt", key, data)
            .status()
            .is_success()
    }
    pub fn start(&self, key: &str) -> bool {
        let mut data = HashMap::new();
        data.insert("SUBID", self.SUBID.clone().unwrap().to_string());
        post_success("https://api.vultr.com/v1/server/start", key, data)
    }
    pub fn destroy(&self, key: &str) -> bool {
        if !confirm(&format!(
            "Destroy server {}? All data will be permanently lost, and the IP address will be released. There is no going back from this action.",
            self.label.clone().unwrap()
        )) {
            return false;
        }
        let mut data = HashMap::new();
        data.insert("SUBID", self.SUBID.clone().unwrap().to_string());
        post_success("https://api.vultr.com/v1/server/destroy", key, data)
    }
    pub fn os_change_list(&self, key: &str) -> Vec<Os> {
        let mut data = HashMap::new();
        data.insert("SUBID", self.SUBID.clone().unwrap().to_string());
        let txt: String =
            auth_get_query("https://api.vultr.com/v1/server/os_change_list", key, data);
        let json: Value = serde_json::from_str(&txt).unwrap();
        let mut oss: Vec<Os> = Vec::new();
        for i in json.as_object().expect("crash").values() {
            let j = i.clone();
            oss.push(from_value(j).expect("deser crash"));
        }
        oss
    }
    pub fn os_change(&self, key: &str, osid: String) -> bool {
        let mut data = HashMap::new();
        data.insert("SUBID", self.SUBID.clone().unwrap().to_string());
        data.insert("OSID", osid);
        post_success("https://api.vultr.com/v1/server/os_change", key, data)
    }
    pub fn app_change_list(&self, key: &str) -> Vec<App> {
        let mut data = HashMap::new();
        data.insert("SUBID", self.SUBID.clone().unwrap().to_string());
        let txt: String =
            auth_get_query("https://api.vultr.com/v1/server/app_change_list", key, data);
        let json: Value = serde_json::from_str(&txt).unwrap();
        let mut apps: Vec<App> = Vec::new();
        for i in json.as_object().expect("crash").values() {
            let j = i.clone();
            apps.push(from_value(j).expect("deser crash"));
        }
        apps
    }
    pub fn app_change(&self, key: &str, appid: String) -> bool {
        let mut data = HashMap::new();
        data.insert("SUBID", self.SUBID.clone().unwrap().to_string());
        data.insert("APPID", appid);
        post_success("https://api.vultr.com/v1/server/app_change", key, data)
    }
    pub fn plan_change_list(&self, key: &str) -> Vec<Plan> {
        let mut data = HashMap::new();
        data.insert("SUBID", self.SUBID.clone().unwrap().to_string());
        let txt: String = auth_get_query(
            "https://api.vultr.com/v1/server/upgrade_plan_list",
            key,
            data,
        );
        let json: Value = serde_json::from_str(&txt).unwrap();
        let mut planid_list: Vec<u64> = Vec::new();
        for i in json.as_array().expect("crash").iter() {
            let j = i.clone();
            planid_list.push(from_value(j).expect("deser crash"));
        }
        let plans = plan::plans_matching_ids(key, planid_list);
        plans
    }
    pub fn plan_change(&self, key: &str, vpsplanid: String) -> bool {
        let mut data = HashMap::new();
        data.insert("SUBID", self.SUBID.clone().unwrap().to_string());
        data.insert("VPSPLANID", vpsplanid);
        post_success("https://api.vultr.com/v1/server/upgrade_plan", key, data)
    }
    pub fn show_root_pass(&self) -> bool {
        let default_password = self.default_password.clone();
        if let Some(pass) = default_password {
            println!("Default Root Password: {}", pass);
            return true;
        }
        return false;
    }
    pub fn show_kvm_url(&self) -> bool {
        let kvm_url = self.kvm_url.clone();
        if let Some(url) = kvm_url {
            println!("KVM URL: {}", url);
            return true;
        }
        return false;
    }
}
