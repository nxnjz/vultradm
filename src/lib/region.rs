//use crate::lib::http;
use crate::cli;
use crate::lib::func::s;
use crate::lib::http;
use serde::{Deserialize, Serialize};
use serde_json::{from_value, Value};
use std::collections::HashMap;

//pub fn select_region() -> String {
//let regions =

#[derive(Serialize, Deserialize, Clone, Debug)]
#[allow(non_snake_case)]
pub struct Region {
    pub DCID: String,
    name: String,
    country: String,
    continent: String,
    state: String,
    ddos_protection: bool,
    block_storage: bool,
    regioncode: String,
    pub available_plans: Option<Vec<u64>>,
}

impl Region {
    pub fn display_string(&self) -> String {
        let name = &self.name;
        let country = &self.country;
        let ddos = match self.ddos_protection {
            true => "[DDOS]",
            false => "",
        };
        let blk = match self.block_storage {
            true => "[BLK STO]",
            false => "",
        };
        let dcid = &self.DCID;
        format!("{} {} {} {} ({})", name, country, ddos, blk, dcid)
    }
    pub fn short_display_string(&self) -> String {
        let name = &self.name;
        let country = &self.country;
        let dcid = &self.DCID;
        format!("{} {} ({})", name, country, dcid)
    }
    pub fn get_avail_plans(&self, key: &str) -> Vec<u64> {
        let dcid = self.DCID.clone();
        let mut data = HashMap::new();
        data.insert("DCID", dcid);
        println!("Fetching Available Plans for Region {}", self.name);
        let txt = http::auth_get_query("https://api.vultr.com/v1/regions/availability", key, data);
        let mut json: Value = serde_json::from_str(&txt).unwrap();
        let plans: Vec<u64> = json
            .as_array_mut()
            .unwrap()
            .drain(..)
            .map(|x| from_value(x).unwrap())
            .collect();
        return plans;
    }
    pub fn ddos_avail(&self) -> bool {
        self.ddos_protection.clone()
    }
    pub fn blksto_avail(&self) -> bool {
        self.block_storage.clone()
    }
}

pub fn get_regions(key: Option<&str>) -> Vec<Region> {
    let region_list: String = match key {
        Some(keystr) => http::auth_get("https://api.vultr.com/v1/regions/list", keystr),
        None => http::pub_get("https://api.vultr.com/v1/regions/list"),
    };
    let json: Value = serde_json::from_str(&region_list).unwrap();
    let mut regions = Vec::new();
    for i in json.as_object().expect("crash").values() {
        let j = i.clone();
        regions.push(from_value(j).expect("deser crash"));
    }
    regions
}

pub fn select_region(key: &str) -> Region {
    println!("Fetching Available Regions...");
    let regions = get_regions(Some(key));
    let menu: Vec<String> = regions
        .iter()
        .map(|region| region.display_string())
        .collect();
    let selection = cli::selection_menu(&menu, "\nVultrAdm:Servers:Deploy:Select Region");
    let selected = regions[selection].clone();
    selected
}
//    let selection = Select::with_theme(&ColorfulTheme::default())
//        .default(0)
//        .items(&menu[..])
//        .interact()
//        .unwrap();
//
//    match selection {
//        //all virtual
//        0 => print_list_vps(),
//        1 => print_list_vc2(),
//        2 => print_list_vc2z(),
//        3 => print_list_vdc2(),
//        4 => print_list_metal(),
//        _ => (),
//    }
//}
//pub fn get_regions() {
//    let txt: String = http::pub_get("https://api.vultr.com/v1/regions/list".to_string());
//    let json: Value = serde_json::from_str(&txt).unwrap();
//    println!(
//        "{: <8} {: <10} {: <4} {: <8} {}",
//        "ID", "TYPE", "vCPU", "USD/MON", "SPECS"
//    );
//    for value in json.as_object().unwrap().values() {
//        println!(
//            "{: <8} {: <10} {: <4} {: <8} {}",
//            value["VPSPLANID"].as_str().unwrap_or("error"),
//            value["plan_type"]
//                .as_str()
//                .unwrap_or("error")
//                .replace("HIGHFREQUENCY", "HIGHFREQ")
//                .replace("DEDICATED", "DEDICPU"),
//            value["vcpu_count"].as_str().unwrap_or("error"),
//            value["price_per_month"].as_str().unwrap_or("error"),
//            value["name"].as_str().unwrap_or("error"),
//        );
//    }
//}
//pub fn print_list_vc2() {
//    let txt: String = http::pub_get("https://api.vultr.com/v1/plans/list_vc2".to_string());
//    let json: Value = serde_json::from_str(&txt).unwrap();
//    println!("{: <10} {}", "ID", "NAME");
//    for value in json.as_object().unwrap().values() {
//        println!(
//            "{: <8} {: <10} {: <4} {: <8} {}",
//            value["VPSPLANID"].as_str().unwrap_or("error"),
//            value["plan_type"].as_str().unwrap_or("error"),
//            value["vcpu_count"].as_str().unwrap_or("error"),
//            value["price_per_month"].as_str().unwrap_or("error"),
//            value["name"].as_str().unwrap_or("error"),
//        );
//    }
//}
//pub fn print_list_vc2z() {
//    let txt: String = http::pub_get("https://api.vultr.com/v1/plans/list_vc2z".to_string());
//    let json: Value = serde_json::from_str(&txt).unwrap();
//    println!("{: <10} {}", "ID", "NAME");
//    for value in json.as_object().unwrap().values() {
//        println!(
//            "{: <8} {: <10} {: <4} {: <8} {}",
//            value["VPSPLANID"].as_str().unwrap_or("error"),
//            value["plan_type"]
//                .as_str()
//                .unwrap_or("error")
//                .replace("HIGHFREQUENCY", "HIGHFREQ"),
//            value["vcpu_count"].as_str().unwrap_or("error"),
//            value["price_per_month"].as_str().unwrap_or("error"),
//            value["name"].as_str().unwrap_or("error"),
//        );
//    }
//}
//pub fn print_list_vdc2() {
//    let txt: String = http::pub_get("https://api.vultr.com/v1/plans/list_vdc2".to_string());
//    let json: Value = serde_json::from_str(&txt).unwrap();
//    println!("{: <10} {}", "ID", "NAME");
//    for value in json.as_object().unwrap().values() {
//        println!(
//            "{: <8} {: <10} {: <4} {: <8} {}",
//            value["VPSPLANID"].as_str().unwrap_or("error"),
//            value["plan_type"]
//                .as_str()
//                .unwrap_or("error")
//                .replace("DEDICATED", "DEDICPU"),
//            value["vcpu_count"].as_str().unwrap_or("error"),
//            value["price_per_month"].as_str().unwrap_or("error"),
//            value["name"].as_str().unwrap_or("error"),
//        );
//    }
//}
//pub fn print_list_metal() {
//    let txt: String = http::pub_get("https://api.vultr.com/v1/plans/list_baremetal".to_string());
//    let json: Value = serde_json::from_str(&txt).unwrap();
//    println!("{: <10} {}", "ID", "NAME");
//    for value in json.as_object().unwrap().values() {
//        println!(
//            "{: <10} {}",
//            value["METALPLANID"].as_str().unwrap_or("error"),
//            value["name"].as_str().unwrap_or("error")
//        );
//    }
//}
