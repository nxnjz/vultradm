use crate::lib::http;
//use serde::{Deserialize, Serialize};
use serde_json::Value;

pub fn print_list(key: &str) {
    let txt: String = http::auth_get("https://api.vultr.com/v1/backup/list", key);
    let json: Value = serde_json::from_str(&txt).unwrap();
    for value in json.as_object().unwrap().values() {
        println!("Backup ID: {}", value["BACKUPID"]);
    }
}
