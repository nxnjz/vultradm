use crate::cli;
use crate::cli::confirm;
use crate::lib::app::select_app;
use crate::lib::func::{s, sleep};
use crate::lib::http;
use crate::lib::os::select_os;
use crate::lib::plan::select_plan;
use crate::lib::region::select_region;
use crate::lib::snapshot::select_snapshot;
use serde::{Deserialize, Serialize};
use serde_json::{from_str, from_value, Value};
use std::collections::HashMap;
//use tabular::{Row, Table};

static BACKUP_OSID: u64 = 180;
static SNAPSHOT_OSID: u64 = 164;
static APP_OSID: u64 = 186;

//implement this later instead of bool success
enum Success {
    silent_yes,
    silent_no,
    yes,
    no,
}

#[derive(Serialize, Deserialize, Clone)]
#[allow(non_snake_case)]
pub struct Server {
    pub SUBID: Option<String>,
    os: Option<String>,
    main_ip: Option<String>,
    pub label: Option<String>,
    tag: Option<String>,
    ram: Option<String>,
    disk: Option<String>,
    vcpu_count: Option<String>,
    location: Option<String>,
    dcid: Option<String>,
    pub default_password: Option<String>,
    date_created: Option<String>,
    pending_charges: Option<String>,
    status: Option<String>,
    cost_per_month: Option<String>,
    current_bandwidth_gb: Option<f64>,
    allowed_bandwidth_gb: Option<String>,
    netmask_v4: Option<String>,
    gateway_v4: Option<String>,
    power_status: Option<String>,
    server_state: Option<String>,
    VPSPLANID: Option<String>,
    //deprecated
    //v6_main_ip: Option<String>,
    //v6_network_size: Option<String>,
    //v6_network: Option<String>,
    v6_networks: Vec<HashMap<String, String>>,
    internal_ip: Option<String>,
    pub kvm_url: Option<String>,
    auto_backups: Option<String>,
    OSID: Option<String>,
    APPID: Option<String>,
    FIREWALLGROUPID: Option<String>,
}

impl Server {
    fn overview(self) -> String {
        let name = format!(
            "{} {} [{}] [{}]",
            self.label.unwrap_or(s("NO_LABEL")),
            self.main_ip.clone().unwrap_or(s("NO_IP")),
            self.tag.unwrap_or(s("NO_TAG")),
            self.os.unwrap_or(s("NO_OS"))
        );

        let status = format!(
            "Power: {} • Status: {} • Server State: {}",
            self.power_status.unwrap_or(s("N/A")),
            self.status.unwrap_or(s("N/A")),
            self.server_state.unwrap_or(s("N/A"))
        );

        let hw = format!(
            "{} • {}vCPU • {} • {}",
            self.ram.unwrap_or(s("N/A")),
            self.vcpu_count.unwrap_or(s("N/A")),
            self.disk.unwrap_or(s("N/A")),
            self.location.unwrap_or(s("N/A"))
        );

        let net4 = format!(
            "Main IPv4: {} • Netmask: {} • Gateway: {}",
            self.main_ip.unwrap_or(s("N/A")),
            self.netmask_v4.unwrap_or(s("N/A")),
            self.gateway_v4.unwrap_or(s("N/A"))
        );

        format!("\nOVERVIEW: \n{}\n{}\n{}\n{}", name, status, hw, net4)
    }
    fn menu_item(&self) -> String {
        let server_label = &self
            .label
            .clone()
            .unwrap_or("NO_LBL".to_string())
            .replacen("", "‣", 1);
        let sub_status = self.status.clone().unwrap_or("N/A".to_string());
        server_label.clone().truncate(15);
        let menu_item = format!(
            "{: <16}{: <16}{: <24}[{}]",
            server_label,
            self.main_ip.clone().unwrap_or("NO_IP".to_string()),
            self.os.clone().unwrap_or("NO_OS".to_string()),
            self.tag.clone().unwrap_or("NO_TAG".to_string())
        );
        menu_item
    }
    fn all_general_info(&self) -> String {
        //incomplete!!!!!!!!
        let server = self.clone();
        let name = format!(
            "{} {} [{}] • {} (OS ID: {} APP ID: {})",
            server.label.unwrap_or(s("NO_LABEL")),
            server.main_ip.clone().unwrap_or(s("NO_IP")),
            server.tag.unwrap_or(s("NO_TAG")),
            server.os.unwrap_or(s("NO_OS")),
            server.OSID.unwrap_or(s("NO_OSID")),
            server.APPID.unwrap_or(s("NO_APPID")),
        );

        let status = format!(
            "Power: {} • Status: {} • Server State: {}",
            server.power_status.unwrap_or(s("N/A")),
            server.status.unwrap_or(s("N/A")),
            server.server_state.unwrap_or(s("N/A"))
        );

        let hw = format!(
            "{} Memory• {} vCPU • {} Disk",
            server.ram.unwrap_or(s("N/A")),
            server.vcpu_count.clone().unwrap_or(s("N/A")),
            server.disk.clone().unwrap_or(s("N/A")),
        );
        let location = format!(
            "Datacenter: {} • ID: {}",
            server.location.clone().unwrap(),
            server.dcid.clone().unwrap()
        );

        let net4 = format!(
            "Main IPv4: {} • Netmask: {} • Gateway: {}",
            server.main_ip.clone().unwrap_or(s("N/A")),
            server.netmask_v4.clone().unwrap_or(s("N/A")),
            server.gateway_v4.clone().unwrap_or(s("N/A"))
        );
        let subid = format!(
            "Subscription {} created on {}",
            server.SUBID.clone().unwrap_or("0".to_string()),
            server.date_created.clone().unwrap()
        );
        let charges = format!(
            "Pending USD: {} • USD/Month: {}",
            server.pending_charges.clone().unwrap(),
            server.cost_per_month.clone().unwrap()
        );
        let bw = format!(
            "Bandwidth: {}/{} GB",
            server.current_bandwidth_gb.clone().unwrap(),
            server.allowed_bandwidth_gb.clone().unwrap()
        );
        format!(
            "{}\n{}\n{}\n{}\n{}\n{}\n{}\n{}\n",
            subid, charges, name, status, hw, location, net4, bw
        )
    }
}

pub fn servers_view(key: &str) {
    loop {
        println!("Fetching Data...");
        let servers_option = get_servers(key);

        let mut menu = Vec::new();
        menu.push("‣Go back".to_string());
        //let mut servers_sorted_option = servers_option.map(|mut servers|
        //    servers.sort_by(|a, b| {
        //        a.label
        //            .clone()
        //            .unwrap_or(String::from("."))
        //            .partial_cmp(&b.label.clone().unwrap_or(String::from(".")))
        //            .unwrap()
        //    })
        //);
        let servers_sorted_option = servers_option.clone();
        if servers_option.is_some() {
            menu.push("‣Deploy New".to_string());
            for server in servers_sorted_option.clone().unwrap().into_iter() {
                menu.push(server.menu_item());
            }
        } else {
            menu.push("‣Deploy New (You have 0 servers)".to_string());
        }
        let selection = cli::selection_menu(&menu, "\nVultrAdm:Servers");
        if selection == 0 {
            break;
        } else if selection == 1 {
            creation_wizard(key);
        } else {
            single_view(servers_sorted_option.unwrap()[selection - 2].clone(), key);
        }
    }
}

fn single_view(server: Server, key: &str) {
    loop {
        println!("{}", server.clone().overview());
        let menu = [
            "‣Go back",
            "‣Details",
            //2
            "‣Reboot",
            //3
            "‣Reinstall",
            //4
            "‣Start",
            //5
            "‣Poweroff",
            //6
            "‣Destroy",
            //7
            "‣Change OS",
            //8
            "‣Change App",
            //9
            "‣Change VPS Plan",
            //10
            "‣Show Default Root Password",
            //11
            "‣Show Console URL",
        ];
        let menu = menu
            .iter()
            .map(|item| item.to_string())
            .collect::<Vec<String>>();

        let prompt = s("\nVultrAdm:Servers:")
            + &server.label.clone().unwrap_or(s("NO_LABEL"))
            + "("
            + &server.main_ip.clone().unwrap_or(s("NO_IP"))
            + ")";
        let selection = cli::selection_menu(&menu.to_vec(), &prompt);
        //println!("{}", server.SUBID.clone().unwrap());

        if 0 == selection {
            break;
        }
        let action_success = match selection {
            1 => details_view(server.clone(), key),
            2 => server.reboot(key),
            3 => server.reinstall(key),
            4 => server.start(key),
            5 => server.halt(key),
            6 => server.destroy(key),
            7 => change_os_view(server.clone(), key),
            8 => change_app_view(server.clone(), key),
            9 => change_plan_view(server.clone(), key),
            10 => server.show_root_pass(),
            11 => server.show_kvm_url(),
            _ => true,
        };

        if action_success {
            println!("SUCCESS");
        } else {
            println!("ACTION FAILED/CANCELLED");
        }
    }
}

fn details_view(server: Server, key: &str) -> bool {
    println!("{}", server.all_general_info());

    return true;
}
fn creation_wizard(key: &str) {
    //init hashmap for post data
    let mut data = HashMap::new();
    //user selects region
    //then check which plans avail in selected region
    let mut region = select_region(key);
    let mut avail_plans_in_region = region.get_avail_plans(key);
    //user selects another region if region sold out/no avail plans
    while avail_plans_in_region.is_empty() {
        println!("No plans currently available in selected region, select another...");
        region = select_region(key);
        avail_plans_in_region = region.get_avail_plans(key);
    }
    let dcid = region.DCID.clone();
    data.insert("DCID", dcid);
    let plan = select_plan(key, avail_plans_in_region);
    let planid = plan.VPSPLANID.clone();
    data.insert("VPSPLANID", planid);
    let os = select_os(key);
    let osid = os.OSID.clone();
    if osid == APP_OSID {
        println!("Fetching Available 1-click Applications");
        let appid = select_app(key);
        data.insert("APPID", appid);
    } else if osid == SNAPSHOT_OSID {
        println!("Fetching Available Snapshot");
        let snapshotid = select_snapshot(key);
        data.insert("SNAPSHOTID", snapshotid);
    } else if osid == BACKUP_OSID {
        println!("idk what to do, no api docs about creating from backup.");
        return;
    }
    data.insert("OSID", osid.to_string());
    let hostname = cli::input("Hostname");
    data.insert("hostname", hostname.clone());
    let label = cli::input("Label");
    data.insert("label", label.clone());
    let tag = cli::input("Tag");
    data.insert("tag", tag.clone());
    let ipv6_enable = cli::yes_no("Assign IPv6 subnet to server? (if available)");
    data.insert("ipv6_enable", ipv6_enable.clone());
    let auto_backups = cli::yes_no("Enable automatic backups?");
    data.insert("auto_backups", auto_backups.clone());
    let ddos_protection = match region.ddos_avail() {
        true => cli::yes_no("Enable DDOS Protection? (available in selected region)"),
        false => "no".to_string(),
    };
    data.insert("ddos_protection", ddos_protection.clone());
    let confirm_msg = format!("\n\nRegion: {}\nPlan: {}\nOS: {}\nHostname: {}\nLabel: {}\nTag: {}\nIPv6: {}\nAutomatic backups: {}\nDDOS protection: {}\n\nCreate Server?", region.short_display_string(), plan.display_string(), os.display_string(), hostname, label, tag, ipv6_enable, auto_backups, ddos_protection);
    if !confirm(&confirm_msg) {
        return;
    }
    let mut response = http::api_req(
        http::Method::post,
        "https://api.vultr.com/v1/server/create",
        Some(key),
        Some(data),
    );
    let response_json = from_str::<Value>(response.text().unwrap().as_str()).expect("serde panic");

    let subid = response_json
        .as_object()
        .expect("serde panic")
        .get("SUBID")
        .expect("serde panic")
        .as_str()
        .expect("serde panic");
    println!("Server created successfully with SUBID: {}", subid);
}

fn change_os_view(server: Server, key: &str) -> bool {
    if !confirm(&format!(
        "Change OS of server {}? All data will be lost.",
        server.label.clone().unwrap()
    )) {
        return false;
    }
    let mut oslist = server.os_change_list(key);
    let prompt = s("\nVultrAdm:Servers:")
        + &server.label.clone().unwrap_or(s("NO_LABEL"))
        + "("
        + &server.main_ip.clone().unwrap_or(s("NO_IP"))
        + "):OS LIST";
    let mut menu = Vec::new();
    for os in oslist.iter() {
        menu.push(format!("{} ({})", os.name, os.OSID));
    }
    let selection = cli::selection_menu(&menu, &prompt);
    let new_osid = oslist.remove(selection).OSID;

    server.os_change(key, new_osid.to_string())
}

fn change_app_view(server: Server, key: &str) -> bool {
    if !confirm(&format!(
        "Change App of server {}? All data will be lost.",
        server.label.clone().unwrap()
    )) {
        return false;
    }
    let mut applist = server.app_change_list(key);
    let prompt = s("\nVultrAdm:Servers:")
        + &server.label.clone().unwrap_or(s("NO_LABEL"))
        + "("
        + &server.main_ip.clone().unwrap_or(s("NO_IP"))
        + "):APP LIST";
    let mut menu = Vec::new();
    for app in applist.iter() {
        menu.push(format!("{} ({})", app.deploy_name, app.APPID));
    }
    let selection = cli::selection_menu(&menu, &prompt);
    let new_appid = applist.remove(selection).APPID;

    server.app_change(key, new_appid.to_string())
}

fn change_plan_view(server: Server, key: &str) -> bool {
    if !confirm(&format!(
        "Change Plan of server {}? Server will be rebooted.",
        server.label.clone().unwrap()
    )) {
        return false;
    }
    let mut planlist = server.plan_change_list(&key);
    let prompt = s("\nVultrAdm:Servers:")
        + &server.label.clone().unwrap_or(s("NO_LABEL"))
        + "("
        + &server.main_ip.clone().unwrap_or(s("NO_IP"))
        + "):CHANGE PLAN";
    let mut menu = Vec::new();
    for plan in planlist.iter() {
        menu.push(plan.display_string());
    }

    let selection = cli::selection_menu(&menu, &prompt);
    let new_planid = planlist.remove(selection).VPSPLANID;

    let plan_change_success = server.plan_change(&key, new_planid.to_string());
    if plan_change_success {
        println!("Rebooting server in 30 seconds...");
        sleep(30);
        if server.reboot(key) {
            println!("Reboot success");
        } else {
            println!("Reboot failed, but is required to complete the upgrade.");
        }
    }
    plan_change_success
}
fn get_servers(key: &str) -> Option<Vec<Server>> {
    //let txt: String = http::auth_get("https://api.vultr.com/v1/server/list", key);
    let txt: String = http::api_req(
        http::Method::get,
        "https://api.vultr.com/v1/server/list",
        Some(key),
        None,
    )
    .text()
    .unwrap_or("error".to_string());
    if txt.trim().trim_matches(|x| "[]".contains(x)).is_empty() {
        return None;
    }
    println!("txt: {}", txt);
    let servers: Vec<Server> = serde_json::from_str::<Value>(&txt)
        .unwrap()
        .as_object()
        .expect("crash")
        .values()
        .map(|i| from_value(i.clone()).expect("deser crash"))
        .collect();
    Some(servers)
}
