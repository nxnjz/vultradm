use crate::cli;
use crate::lib::http;
use serde::{Deserialize, Serialize};
use serde_json::{from_value, Value};

//pub fn print_list() {
//    let txt: String = http::pub_get("https://api.vultr.com/v1/app/list".to_string());
//    //println!("{}", txt);
//    let json: Value = serde_json::from_str(&txt).unwrap();
//    //println!("{:?}", json["1"]);
//    //println!("{:?}", json.as_object().unwrap());
//    println!("{: <10} {}", "APPID", "NAME");
//    for value in json.as_object().unwrap().values() {
//        println!(
//            "{: <10} {}",
//            value["APPID"].as_str().unwrap_or("error"),
//            value["deploy_name"].as_str().unwrap_or("error")
//        );
//    }
//}

#[derive(Serialize, Deserialize, Clone)]
#[allow(non_snake_case)]
pub struct App {
    pub APPID: String,
    pub deploy_name: String,
}

impl App {
    pub fn display_string(&self) -> String {
        format!("{} ({})", self.deploy_name, self.APPID)
    }
}

pub fn get_app(_key: &str) -> Vec<App> {
    let txt: String = http::pub_get("https://api.vultr.com/v1/app/list");
    let mut json: Value = serde_json::from_str(&txt).unwrap();
    let app_list: Vec<App> = json
        .as_object_mut()
        .unwrap()
        .values_mut()
        .map(|app| from_value(app.clone()).expect("deser crash"))
        .collect();
    app_list
}

pub fn select_app(key: &str) -> String {
    let app_list = get_app(key);
    let menu: Vec<String> = app_list.iter().map(|app| app.display_string()).collect();
    let selection = cli::selection_menu(&menu, "\nVultrAdm:Servers:Deploy:Select OS");
    return app_list[selection].APPID.to_string();
}
//pub fn cmd_apps_search(search_str) {
//let txt: String = http::pub_get("https://api.vultr.com/v1/app/list".to_string());
//let json: Value = serde_json::from_str(&txt).unwrap();
//println!("{: <10} {}", "APPID", "NAME");
//for value in json.as_object().unwrap().values() {
////if value.as_str().contains("
//println!(
//"{: <10} {}",
//value["APPID"].as_str().unwrap_or("error"),
//value["deploy_name"].as_str().unwrap_or("error")
//);
//}
//}
