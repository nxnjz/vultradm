use crate::cli;
use crate::lib::http;
use serde::{Deserialize, Serialize};
use serde_json::{from_value, map::Map, Value};
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Clone)]
#[allow(non_snake_case)]
pub struct SSHKey {
    pub SSHKEYID: Option<String>,
    date_created: Option<String>,
    name: Option<String>,
    ssh_key: Option<String>,
}

impl SSHKey {
    fn menu_item(&self) -> String {
        let menu_item = format!(
            "‣ [{}] [{}] {}",
            self.SSHKEYID.as_ref().unwrap_or(&"n/a KEY ID".to_string()),
            self.date_created
                .as_ref()
                .unwrap_or(&"n/a DATE CREATED".to_string()),
            self.name.as_ref().unwrap_or(&"n/a KEY NAME".to_string())
        );
        menu_item
    }
}

fn create(key: &str, ssh_key_name: String, ssh_key: String) -> Result<String, String> {
    let mut data = HashMap::new();
    data.insert("name", ssh_key_name);
    data.insert("ssh_key", ssh_key);
    return Ok("".to_string());
}

fn list(key: &str) -> Result<Vec<SSHKey>, serde_json::error::Error> {
    let txt: String = http::auth_get("https://api.vultr.com/v1/sshkey/list", key);
    println!("txt: {}", txt);
    let json: Value = serde_json::from_str(&txt).unwrap();
    //println!("val: {}", json);
    let ssh_keys: Vec<SSHKey> = json
        .as_object()
        .expect("expected json obj got else")
        .values()
        .map(move |value| from_value(value.clone()).expect("Deser error"))
        .collect();
    //let ssh_keys: Vec<SSHKey> = serde_json::from_str(&str).unwrap();
    Ok(ssh_keys)
}

fn creation_wizard(key: &str) {
    let name = cli::input("Name");
    let ssh_key = cli::input("Paste SSH Public Key");
    let mut data = HashMap::new();
    data.insert("name", name);
    data.insert("ssh_key", ssh_key);
    let mut response = http::api_req(
        http::Method::post,
        "https://api.vultr.com/v1/sshkey/create",
        Some(key),
        Some(data),
    );
    let response_json: Value =
        serde_json::from_str(&response.text().unwrap()).expect("deser error");
    let key_id = response_json.as_object().expect("expected obj got else")["SSHKEYID"];
    println!("Key {} created with ID {}", name, key_id);
}

pub fn keys_view(key: &str) {
    loop {
        println!("Fetching data...");
        let keys_res = list(key);

        let mut menu = Vec::new();
        menu.push("‣ Go back".to_string());
        menu.push("‣ Add new".to_string());

        if keys_res.is_err() {
            println!("Error Fetching SSH Keys");
            return;
        }

        let keys = keys_res.unwrap();

        for key in keys.iter() {
            menu.push(key.menu_item());
        }

        let selection = cli::selection_menu(&menu, "\nVultrAdmin:SSHKeys");
        match selection {
            0 => break,
            1 => creation_wizard(key),
            _ => (),
        }
        if selection == 0 {
            break;
        } else if selection == 1 {
            creation_wizard(key);
        } else {
            single_view(key, keys[selection - 2])
        }
    }
}
