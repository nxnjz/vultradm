use crate::cli;
use crate::lib::http;
use serde::{Deserialize, Serialize};
use serde_json::{from_value, Value};

//pub fn print_list() {
//    let txt: String = http::pub_get("https://api.vultr.com/v1/os/list".to_string());
//    let json: Value = serde_json::from_str(&txt).unwrap();
//    println!("{: <10} {}\n", "ID", "NAME");
//    for value in json.as_object().unwrap().values() {
//        println!(
//            "{: <10} {}",
//            value["OSID"].as_i64().unwrap_or(0),
//            value["name"].as_str().unwrap_or("error")
//        );
//    }
//}

#[derive(Serialize, Deserialize, Clone)]
#[allow(non_snake_case)]
pub struct Os {
    pub OSID: u64,
    pub name: String,
}

impl Os {
    pub fn display_string(&self) -> String {
        format!("{} ({})", self.name, self.OSID)
    }
}

pub fn get_os(_key: &str) -> Vec<Os> {
    let txt: String = http::pub_get("https://api.vultr.com/v1/os/list");
    let mut json: Value = serde_json::from_str(&txt).unwrap();
    let os_list: Vec<Os> = json
        .as_object_mut()
        .unwrap()
        .values_mut()
        .map(|os| from_value(os.clone()).expect("deser crash"))
        .collect();
    os_list
}

pub fn select_os(key: &str) -> Os {
    println!("Fetching Available OS images");
    let mut os_list = get_os(key);
    let menu: Vec<String> = os_list.iter().map(|os| os.display_string()).collect();
    let selection = cli::selection_menu(&menu, "\nVultrAdm:Servers:Deploy:Select OS");
    return os_list.remove(selection);
}
