use crate::cli;
use crate::lib::http;
use serde::{Deserialize, Serialize};
use serde_json::{from_value, Value};

#[derive(Serialize, Deserialize, Clone)]
#[allow(non_snake_case)]
pub struct Snapshot {
    SNAPSHOTID: String,
    date_created: String,
    description: String,
    size: String,
    status: String,
    OSID: String,
    APPID: String,
}

impl Snapshot {
    pub fn display_string(&self) -> String {
        format!("[{}] {}", self.date_created, self.description)
    }
}

pub fn get_snapshot(key: &str) -> Vec<Snapshot> {
    let txt: String = http::auth_get("https://api.vultr.com/v1/snapshot/list", key);
    let mut json: Value = serde_json::from_str(&txt).unwrap();
    let snapshot_list: Vec<Snapshot> = json
        .as_object_mut()
        .unwrap()
        .values_mut()
        .map(|snapshot| from_value(snapshot.clone()).expect("deser crash"))
        .collect();
    snapshot_list
}

pub fn select_snapshot(key: &str) -> String {
    let snapshot_list = get_snapshot(key);
    let menu: Vec<String> = snapshot_list
        .iter()
        .map(|snapshot| snapshot.display_string())
        .collect();
    let selection = cli::selection_menu(&menu, "\nVultrAdm:Servers:Deploy:Select Snapshot");
    return snapshot_list[selection].SNAPSHOTID.to_string();
}
