use clap::{App, Arg};
use std::{thread, time};
//use clap::SubCommand;
use dialoguer::Confirmation;
use dialoguer::Input;
use dialoguer::{theme::ColorfulTheme, Select};

pub fn app<'a, 'b>() -> clap::App<'a, 'b> {
    let appname = "vultradm";
    let appver = "0.1.0";

    let app = App::new(appname)
        .version(appver)
        .author("Karl W. <karl@nxnjz.net>")
        .about("Human CLI for Vultr API")
        .arg(
            Arg::with_name("Vultr API Key")
                .short("k")
                .long("key")
                .value_name("api_key")
                .help("Vultr API Key. Can also be set in $VULTR_API_KEY or in key.txt")
                .takes_value(true),
        );

    return app;
}

pub fn confirm(msg: &str) -> bool {
    Confirmation::new().with_text(msg).interact().unwrap()
}

pub fn yes_no(msg: &str) -> String {
    if confirm(msg) {
        return "yes".to_string();
    } else {
        return "no".to_string();
    }
}

pub fn input(prompt: &str) -> String {
    Input::<String>::new()
        .with_prompt(prompt)
        .allow_empty(true)
        .interact()
        .unwrap()
}

pub fn selection_menu<T: ToString>(items: &Vec<T>, prompt: &str) -> usize {
    let items = items
        .iter()
        .map(|item| item.to_string())
        .collect::<Vec<String>>();
    let selection = Select::with_theme(&ColorfulTheme::default())
        .default(0)
        .with_prompt(prompt)
        .items(&items[..])
        .interact()
        .unwrap();
    selection
}

pub fn sleep_with_msg(secs: u64, msg: &str) {
    let dur = time::Duration::from_secs(secs);
    println!("{}", msg);
    thread::sleep(dur);
}
